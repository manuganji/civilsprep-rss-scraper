$(document).ready(function(){
    //$('.addContent').on('click', function() {
    $(document).on("click", "button.addContent", function(){   
        $(this).parent().addClass('selectedContent').attr('style','border:1px solid red');
        $(this).remove();
    });

    $('.getContent').click(function(){
        $('.feedItemOuter').each(function() {
            $(this).attr('style','');
            if(!$(this).hasClass('selectedContent')) {
                $(this).remove();
            }
        });

        $('.rssContainer').each(function() {
            if($(this).find('.feedItemOuter').length == 0) {
                $(this).remove();
            }
        });

        //Removing unnecessary p and br tags from the output that may affect the ui layout
        $('p,br').each(function() {
            $(this).remove();
        });
        $('.output').html($('#news_post').html());
        
        
    });
    $(document).on("click", ".toggle_left", function(){
        $(this).parent().parent('.rssContainer').addClass('floatLeft').removeClass('floatRight');     
    });
    $(document).on("click", ".toggle_right", function(){
        $(this).parent().parent('.rssContainer').addClass('floatRight').removeClass('floatLeft');     
    });
    $('.styleContent').click(function(){
        $("#news_post button").remove();
        if($('#radio').attr('href').trim().length == 0){
            $('#radio').remove();
        }
        $('.output').html($('#news_post').html());
    });
    $('#embed_link').change(function(){
        $('iframe').replaceWith($('#embed_link').val());

    });
    $('#video_title').change(function(){
        $('#vid').text($('#video_title').val());
    });
    $('#embed_radio').change(function(){
        $('#radio').attr('href', $('#embed_radio').val());
    });
});
