$(function(){
    feeds = {
                toi: 'http://pipes.yahoo.com/pipes/pipe.run?_id=3dd385ae9642d4e3da982c7758a9d20f&_render=json&latestdate=3+days+ago',
                hindu: 'http://pipes.yahoo.com/pipes/pipe.run?_id=1a3129140ed258089c6906caf786e7b6&_render=json&latestby=3+days+ago',
                ie: 'http://pipes.yahoo.com/pipes/pipe.run?_id=ebc18c8657f6708276259346013191b2&_render=json&latestby=2+days+ago',
                dte: 'http://pipes.yahoo.com/pipes/pipe.run?_id=ea05d52788502dac38ec538dcc820c86&_render=json&latestby=3+days+ago',
                epw: 'http://pipes.yahoo.com/pipes/pipe.run?_id=a25993b545444896cd3895493add1d39&_render=json&latestby=3+days+ago',
                pib: 'http://pipes.yahoo.com/pipes/pipe.run?_id=59d1255eca7db6beb026e3a9c859dfce&_render=json&latestby=3+days+ago',
            }
    
    // this acts as a template to create feed items
    get_feed_item = function(item_title, item_link, item_description){
        
        item_html = "<div class=\"feedItemOuter  \">" ;
        item_html += "<div class=\"feedItemTitle\"><a href="+ item_link + " btarget=\"_blank\">"+ item_title + "</a></div>";
        item_html += "<button class=\"addContent\" type=\"button\"> Add »</button>"
        item_html += "<div class=\"feedItemDesc\">"
        if(item_description !== null){
            item_html += item_description
        } 
        item_html += "</div>";
        item_html += "</div>";
        return item_html;
    }

    // a template to create the feed containers
    get_feed_container = function(feed_title, feed_link, feed_items){
        if(feed_items.length != 0){
            feed_html = "<div class=\"rssContainer floatLeft\">";
            feed_html += "<div class=\"channelTitle\">";
            feed_html += "<a href=\""+ feed_link +" target=\"_blank\">"+ feed_title +"</a>";
            feed_html += "<button class=\"toggle_right\"> Set Right </button>";
            feed_html += "<button class=\"toggle_left\"> Set Left </button>";
            feed_html += "</div>";
            $.each(feed_items, function(index, item){ 
                feed_html += get_feed_item(item['title'], item['link'], item['description']);
            });
            feed_html += "</div>"
            return feed_html;
        }else{
            console.log("no feed_items for" + feed_title);
            return false;
        }
    };

    render_feed = function(resp){
        html = get_feed_container(resp.value.title, resp.value.description, resp.value.items);
        $('.rssFeedSetOfDay').append(html);
    }
    $.each(feeds, function(index, link_to_json){
        $.ajax({url:link_to_json, success:render_feed});
    });

});
